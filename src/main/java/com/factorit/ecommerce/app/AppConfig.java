package com.factorit.ecommerce.app;

import java.util.ArrayList;
import java.util.List;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import com.factorit.ecommerce.app.models.domain.Client;
import com.factorit.ecommerce.app.models.domain.Purchase;

@Configuration
public class AppConfig {

	@Bean("clients")
	public List<Client> registerClients(){
		List<Client> clients = new ArrayList<Client>();
		clients.add(new Client("85495753", false));
		clients.add(new Client("35357654", false));
		clients.add(new Client("40634851", false));
		clients.add(new Client("99852741", false));
		clients.add(new Client("11486624", false));
		return clients;
	}
	
	@Bean("purchases")
	public List<Purchase> purchases(){
		List<Purchase> purchases = new ArrayList<Purchase>();
		purchases.add(new Purchase("40634851",300));
		purchases.add(new Purchase("40634851",400));
		purchases.add(new Purchase("40634851",500));
		purchases.add(new Purchase("99852741",1000));
		purchases.add(new Purchase("99852741",1500));
		
		return purchases;
	}
}
