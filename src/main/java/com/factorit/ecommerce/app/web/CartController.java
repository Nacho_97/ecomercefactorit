package com.factorit.ecommerce.app.web;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.factorit.ecommerce.app.models.domain.Cart;
import com.factorit.ecommerce.app.models.domain.Client;
import com.factorit.ecommerce.app.models.domain.Product;
import com.factorit.ecommerce.app.models.domain.Promotion;
import com.factorit.ecommerce.app.models.domain.Purchase;
import com.factorit.ecommerce.app.models.domain.Shop;
import com.google.gson.Gson;

@Controller
@RequestMapping("/cart")
public class CartController {
	
	@Autowired
	private Shop shop;
	
	@Autowired
	private PurchaseController purchaseController;
	
	@Autowired
	private ClientController clientController;
	
	@PostMapping("/createCart")
	@ResponseBody
	public String createCart(@RequestParam(name = "isSpecial", defaultValue = "false") boolean isSpecial) {
		String msg = "[createCart] cart created";
		shop.createCart(isSpecial);
		System.out.println(msg);
		return msg;
	}
	
	@PostMapping("/deleteCart")
	@ResponseBody
	public String deleteCart() {
		String msg = "[deleteCart] cart deleted";
		shop.deleteCart();
		System.out.println(msg);
		return msg;
	}
	
	@PostMapping("/addProduct")
	@ResponseBody
	public String addProduct(@RequestBody Product product, 
			@RequestParam(name = "quantity", defaultValue = "0") Long quantity) {
		String msg = "[addProduct] ";
		Cart cart = shop.getCart();
		if(cart != null) {
			cart.addItem(product, quantity);
			msg += "product added ".concat(shop.getCart().toString());
		}else {
			msg += "product not added, the cart not exist";	
		}
		System.out.println(msg);
		return msg;
	}
	
	@PostMapping("/removeProduct")
	@ResponseBody
	public String removeProduct(@RequestBody Product product) {
		String msg = "[removeProduct] ";
		Cart cart = shop.getCart();
		if(cart != null) {
			cart.removeItem(product);
			msg += "product removed";
		}else {
			msg += "product not removed, the cart not exist";
		}
		System.out.println(msg);
		return msg;
	}
	
	@GetMapping("/getItems")
	@ResponseBody
	public String getItems() {
		Cart cart = shop.getCart();
		if(cart != null) {
			Gson gson = new Gson();
			return gson.toJson(cart.getItems());
		}
		return "not exist the cart";
	}
	
	@PostMapping("/closeCart")
	@ResponseBody
	public String closeCart(@RequestBody Client client) {
		if(client != null) {
			System.out.println(client.toString());
			Purchase p = shop.buy(client);
			deleteCart();
			if(p != null) {
				purchaseController.addPurchase(p);
				List<Purchase> purchases = purchaseController.getPurchases(client.getIdCard(), dateFirstDayMonth(), false, false);
				System.out.println(purchases.toString());
				boolean isVIP = Promotion.clientIsVIP(purchases);
				clientController.setClientVIP(client, isVIP);
				System.out.println("[closeCart] cart closed, detail of Purchase: "+p.toString());
				Gson gson = new Gson();
				return gson.toJson(p);
			}
		}
		return "[closeCart] client or the cart not exist, purchase not completed";
		
	}
	
	private static Date dateFirstDayMonth() {
		Calendar c = Calendar.getInstance();
	    c.set(Calendar.DAY_OF_MONTH, 1);
	    return c.getTime();
	}
}
