package com.factorit.ecommerce.app.web;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.factorit.ecommerce.app.models.domain.Purchase;

@Controller
@RequestMapping("/purchase")
public class PurchaseController {
	
	@Autowired
	@Qualifier("purchases")
	private List<Purchase> purchases;
	
	@GetMapping("/getByIdCard/{id}")
	@ResponseBody
	public List<Purchase> getPurchases(@PathVariable(value="id") String idCard,
									   @RequestParam(name = "date", defaultValue = "", required = false) @DateTimeFormat(pattern = "dd-MM-yyyy") Date dateUser,
									   @RequestParam(name = "orderByDate", defaultValue = "false", required = false) boolean orderByDate,
									   @RequestParam(name = "orderByAmount", defaultValue = "false", required = false) boolean orderByAmount){
		List<Purchase> result = purchases;
		if(!idCard.isEmpty() && dateUser != null) {
			result = purchases.stream().filter(e -> e.getDate().after(dateUser) && e.getClientIdCard().equals(idCard)).collect(Collectors.toList());
			if(orderByDate) {
				result = result.stream().sorted((e1, e2) -> - e1.getDate().compareTo(e2.getDate())).collect(Collectors.toList());
			}
			if(orderByAmount) {
				result = result.stream().sorted((e1, e2) -> - Double.compare(e1.getTotalAmount(), e2.getTotalAmount())).collect(Collectors.toList());
			}
		}
		return result;
	}
	
	@PostMapping("/addPurchase")
	public void addPurchase(@RequestBody Purchase purchase) {
		purchases.add(purchase);
		System.out.println("[addPurchase] purchase added");
	}
}
