package com.factorit.ecommerce.app.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.factorit.ecommerce.app.models.domain.Client;

@Controller
@RequestMapping("/client")
public class ClientController {
	
	@Autowired
	@Qualifier("clients")
	private List<Client> clients;
	
	@PostMapping("/setClientVIP")
	public void setClientVIP(@RequestBody Client client, @RequestParam boolean isVIP) {
		for(Client c : clients) {
			if(c.equals(client)) {
				System.out.println("");
				c.setVIP(isVIP);
			}
		}
	}

}
