package com.factorit.ecommerce.app.models.domain;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Cart {
	private Set<Item> items;
	private boolean isSpecial;
	private double totalAmount;

	public Cart() {
		this.items = new HashSet<Item>();
		this.isSpecial = false;
		this.totalAmount = 0;
	}
	
	public Cart(boolean isSpecial) {
		this.items = new HashSet<Item>();
		this.isSpecial = isSpecial;
		this.totalAmount = 0;
	}

	public List<Item> getItems() {
		return new ArrayList<Item>(items);
	}
	
	public Item getItem(Product product) {
		for(Item item : getItems())
			if(item.isItem(product))
				return item;
		return null;
	}

	public boolean isSpecial() {
		return isSpecial;
	}

	public void setSpecial(boolean isSpecial) {
		this.isSpecial = isSpecial;
	}

	public double getTotalAmount() {
		return totalAmount;
	}

	public boolean addItem(Product product, Long quantity) {
		Item newItem = new Item(product, quantity);
		if(this.items.contains(newItem)) {
			Item oldItem = getItem(product);
			Long oldQuantity = oldItem.getQuantity();
			newItem.setQuantity(oldQuantity + quantity);
			removeItem(product);
		}
		totalAmount += newItem.getTotalAmount();
		return items.add(newItem);
	}
	
	public boolean removeItem(Product product) {
		Item i = getItem(product);
		if(items.contains(i)) {
			totalAmount -= i.getTotalAmount();
			return items.remove(i);	
		}
		return false;
	}
	
	public double closeCart(boolean clientVIP) {
		double totalDiscount = 0;
		totalDiscount += Promotion.getDiscountClientVip(totalAmount, clientVIP);
		totalDiscount += Promotion.getDiscountFourEqualsProduct(getItems());
		totalDiscount += Promotion.getDiscountThreeEqualProducts(getItems(), this.isSpecial);
		System.out.println("total discount: ".concat(Double.toString(totalDiscount)));
		totalAmount -= totalDiscount;
		return totalAmount;
	}
	
	@Override
	public String toString() {
		return "{Cart [items=" + items + ", isSpecial=" + isSpecial + ", totalAmount=" + totalAmount + "]}";
	}
}
