package com.factorit.ecommerce.app.models.domain;

import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.SessionScope;

@Component
@SessionScope
public class Shop {
	
	private Cart cart;
	
	public Shop() {
		cart = null;
	}
	public Cart getCart() {
		return cart;
	}
	public void setCart(Cart cart) {
		this.cart = cart;
	}
	
	public void createCart(boolean isSpecial) {
		cart = new Cart(isSpecial);
	}
	
	public void deleteCart() {
		cart = null;
	}
	
	public Purchase buy(Client client) {
		if(cart != null) {
			double finalAmount = cart.closeCart(client.isVIP());
			Purchase p = new Purchase(client.getIdCard(), finalAmount);
			return p;
		}
		return null;
	}

}
