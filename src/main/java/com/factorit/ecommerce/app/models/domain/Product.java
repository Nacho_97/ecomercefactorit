package com.factorit.ecommerce.app.models.domain;

import java.util.Objects;

public class Product {
	private double amount;
	private String name;

	public Product(double amount, String name) {
		isAValidPrice(amount);
		this.amount = amount;
		this.name = name;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	private void isAValidPrice(double price) {
		if (price <= 100) {
			throw new RuntimeException("The price of the product must be greater than $ 100");
		}
	}

	@Override
	public int hashCode() {
		return Objects.hash(amount, name);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Product other = (Product) obj;
		return Double.doubleToLongBits(amount) == Double.doubleToLongBits(other.amount)
				&& Objects.equals(name, other.name);
	}

	@Override
	public String toString() {
		return "Product [amount=" + amount + ", name=" + name + "]";
	}
}
