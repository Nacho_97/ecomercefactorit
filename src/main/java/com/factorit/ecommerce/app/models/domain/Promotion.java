package com.factorit.ecommerce.app.models.domain;

import java.util.List;
import java.util.Optional;

public class Promotion {
	
	public static double getDiscountThreeEqualProducts(List<Item> items, boolean cartSpecial) {
		if(items != null) {
			Long quantityProducts = items.stream().mapToLong(e -> e.getQuantity()).sum();
			double ret = quantityProducts > 3 ? cartSpecial ? 150 : 100 : 0;
			System.out.println("[ThreeEqualProducts] three equal products $-"+ret);
			return ret;
		}
		return 0;
	}
	
	public static boolean clientIsVIP(List<Purchase> purchases) {
		double sum = purchases.stream().mapToDouble(e -> e.getTotalAmount()).sum();
		return sum > 5000? true : false;
	}
	
	public static double getDiscountClientVip(double totalAmount, boolean clientIsVIP) {
		System.out.println("totalAmount "+totalAmount+" clientIsVIP "+clientIsVIP);
		if(totalAmount > 2000 && clientIsVIP) {
			System.out.println("[DiscountClientVip] client VIP $-500.0");
			return 500;
		}
		System.out.println("[DiscountClientVip] client VIP $-0.0");
		return 0;
	}
	
	public static double getDiscountFourEqualsProduct(List<Item> items) {
		Optional<Item> item = items.stream().filter(e -> e.getQuantity() == 4).findFirst();
		if(item.isPresent()) {
			Item i = item.get();
			System.out.println("[DiscountFourEqualsProduct] four equals product $-"+i.getProduct().getAmount());
			return i.getProduct().getAmount();
		}else {
			System.out.println("[DiscountFourEqualsProduct] four equals product $-0.0");
			return 0;
		}
	}
}
