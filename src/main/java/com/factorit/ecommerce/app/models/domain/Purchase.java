package com.factorit.ecommerce.app.models.domain;

import java.util.Date;

public class Purchase {

	private String ClientIdCard;
	private Date date;
	private double totalAmount;

	public Purchase() {
		ClientIdCard = "";
		date = new Date();
		totalAmount = 0;
	}

	public Purchase(String clientIdCard, double totalAmount) {
		ClientIdCard = clientIdCard;
		date = new Date();
		this.totalAmount = totalAmount;
	}

	public String getClientIdCard() {
		return ClientIdCard;
	}

	public void setClientIdCard(String clientIdCard) {
		ClientIdCard = clientIdCard;
	}

	public Date getDate() {
		return date;
	}

	public double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}

	@Override
	public String toString() {
		return "Purchase [ClientIdCard=" + ClientIdCard + ", date=" + date + ", totalAmount=" + totalAmount + "]";
	}
}
