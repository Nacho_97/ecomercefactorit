package com.factorit.ecommerce.app.models.domain;

import java.util.Objects;

public class Item {
	private Product product;
	private Long quantity;
	
	public Item(Product product, Long quantity) {
		this.product = product;
		this.quantity = quantity;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Long getQuantity() {
		return quantity;
	}

	public void setQuantity(Long quantity) {
		this.quantity = quantity;
	}
	
	public double getTotalAmount() {
		return product.getAmount() * quantity;
	}
	
	public boolean isItem(Product product) {
		return this.product.equals(product);
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(product);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Item other = (Item) obj;
		return Objects.equals(product, other.product);
	}
	
	@Override
	public String toString() {
		return "Item [product=" + product + ", quantity=" + quantity + "]\n";
	}
}
