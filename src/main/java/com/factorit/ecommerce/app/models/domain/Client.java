package com.factorit.ecommerce.app.models.domain;

public class Client {


	private String idCard;
	private boolean isVIP;
	
	public Client() {
		this.idCard = "";
		isVIP = false;
	}

	public Client(String idCard, boolean isVIP) {
		this.idCard = idCard;
		this.isVIP = isVIP;
	}

	public String getIdCard() {
		return idCard;
	}

	public void setIdCard(String idCard) {
		this.idCard = idCard;
	}

	public boolean isVIP() {
		return isVIP;
	}

	public void setVIP(boolean isVIP) {
		this.isVIP = isVIP;
	}
	
	public boolean equals(Object obj) {
		boolean ret = false;
		if (this == obj)
	        return true;
		if (obj == null)
	        return false;
		if(obj instanceof Client) {
			Client anotherClient = (Client) obj;
			ret = this.idCard.equals(anotherClient.getIdCard());
		}
		return ret;
	}

	@Override
	public String toString() {
		return "Client [idCard=" + idCard + ", isVIP=" + isVIP + "]";
	}
}
